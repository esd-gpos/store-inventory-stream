package item_stream

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoOperateRepo struct {
	Col *mongo.Collection
}

func (m *MongoOperateRepo) Insert(ctx context.Context, itemEntity *ItemEntity) error {
	if _, err := m.Col.InsertOne(ctx, itemEntity); err != nil {
		return err
	}

	return nil
}

func (m *MongoOperateRepo) Delete(ctx context.Context, itemId string) error {
	if _, err := m.Col.DeleteOne(ctx, bson.M{"_id": itemId}); err != nil {
		return err
	}

	return nil
}
