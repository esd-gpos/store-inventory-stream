package item_stream

import (
	"bytes"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

const (
	opInsert = "insert"
	opDelete = "delete"
)

// Represent item
type ItemEntity struct {
	ItemId    string `bson:"_id"`
	ProductId string `bson:"product_id"`
	StoreId   string `bson:"store_id"`
	UserId    string `bson:"user_id"`
}

// Represent information from Mongo Change Stream
type MongoStreamEntity struct {
	FullDocument  ItemEntity        `bson:"fullDocument"`
	DocumentKey   DocumentKeyEntity `bson:"documentKey"`
	OperationType string            `bson:"operationType"`
}

type DocumentKeyEntity struct {
	StoreId string `bson:"_id"`
}

// Is used to operate on the data sources
type OperateRepo interface {
	Insert(ctx context.Context, itemEntity *ItemEntity) error

	Delete(ctx context.Context, itemId string) error
}

// Persist the resume token to some storage
type TokenRepo interface {
	Write(ctx context.Context, token []byte) error
	Token(ctx context.Context) ([]byte, error)
}

// Watch MongoDB Change Stream
func WatchMongoStream(ctx context.Context, col *mongo.Collection, operate OperateRepo, token TokenRepo) error {
	// Retrieve the latest resume token first
	log.Print("Retrieving resume token")

	tk, err := token.Token(ctx)
	if err != nil {
		return err
	}

	// Create ChangeStream
	log.Println("Creating Change Stream")

	ops := make([]*options.ChangeStreamOptions, 0)
	if len(tk) != 0 {
		temp, err := bson.NewFromIOReader(bytes.NewReader(tk))
		if err != nil {
			return err
		}

		ops = append(ops, options.ChangeStream().SetResumeAfter(temp))
	}

	cs, err := col.Watch(ctx, mongo.Pipeline{}, ops...)
	if err != nil {
		return err
	}

	// Start reading stream
	log.Println("Start watching stream")

	for cs.Next(ctx) {
		// Decode into entity
		streamEntity := &MongoStreamEntity{}

		err := cs.Decode(streamEntity)
		if err != nil {
			return err
		}

		// Propagate update
		if streamEntity.OperationType == opInsert {
			if err = operate.Insert(ctx, &streamEntity.FullDocument); err != nil {
				return err
			}

			log.Printf("Store %s inserted\n", streamEntity.FullDocument.StoreId)
		} else if streamEntity.OperationType == opDelete {
			if err = operate.Delete(ctx, streamEntity.DocumentKey.StoreId); err != nil {
				return err
			}

			log.Printf("Store %s deleted\n", streamEntity.DocumentKey.StoreId)
		}

		// Update token
		if err = token.Write(ctx, cs.ResumeToken()); err != nil {
			return err
		}
	}

	return nil
}
