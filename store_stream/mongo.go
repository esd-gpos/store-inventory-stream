package store_stream

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoOperateRepo struct {
	Col *mongo.Collection
}

func (m *MongoOperateRepo) Insert(ctx context.Context, storeEntity *StoreEntity) error {
	if _, err := m.Col.InsertOne(ctx, storeEntity); err != nil {
		return err
	}

	return nil
}

func (m *MongoOperateRepo) Set(ctx context.Context, storeEntity *StoreEntity) error {
	if _, err := m.Col.UpdateOne(ctx, bson.M{"_id": storeEntity.StoreId, "user_id": storeEntity.UserId}, bson.M{
		"$set": bson.M{"token": storeEntity.Token},
	}); err != nil {
		return err
	}

	return nil
}

func (m *MongoOperateRepo) Delete(ctx context.Context, storeId string) error {
	if _, err := m.Col.DeleteOne(ctx, bson.M{"_id": storeId}); err != nil {
		return err
	}

	return nil
}
