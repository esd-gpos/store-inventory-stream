package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/esd-gpos/store-inventory-stream/store_stream"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strings"
	"time"
)

const (
	storeFilename = "store-token"
	configFile    = "main.env"
	configPath    = "."
)

func main() {
	// Load config
	loadConfig(configFile, configPath)

	// Get env variables
	mongoStoreItemUri := viper.GetString("MONGO_STORE_ITEM_URI")
	mongoStoreItemDb := viper.GetString("MONGO_STORE_ITEM_DB")
	mongoStoreCol := viper.GetString("MONGO_STORE_COLLECTION")

	mongoTranUri := viper.GetString("MONGO_TRAN_URI")
	mongoTranDb := viper.GetString("MONGO_TRAN_DB")
	mongoTranStoreCol := viper.GetString("MONGO_TRAN_STORE_COLLECTION")

	failOnEmpty(mongoStoreItemUri, mongoStoreItemDb, mongoStoreCol, mongoTranUri, mongoTranDb, mongoTranStoreCol)

	// Connect to Mongo Store and Item collection
	mongoStoreItemClient, err := mongo.NewClient(options.Client().ApplyURI(mongoStoreItemUri))
	failOnError("could not create mongo client", err)

	timeout, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoStoreItemClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoStoreCollection := mongoStoreItemClient.Database(mongoStoreItemDb).Collection(mongoStoreCol)

	// Connect to Mongo Transaction Store and Item collection
	mongoTranClient, err := mongo.NewClient(options.Client().ApplyURI(mongoTranUri))
	failOnError("could not create mongo client", err)

	timeout, cancel = context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoTranClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoTranStoreCollection := mongoTranClient.Database(mongoTranDb).Collection(mongoTranStoreCol)

	// Create Repo
	operateStore := &store_stream.MongoOperateRepo{Col: mongoTranStoreCollection}
	fileStoreToken := &store_stream.FileTokenRepo{Filename: storeFilename}

	log.Println("Begin")
	log.Fatal(store_stream.WatchMongoStream(context.Background(), mongoStoreCollection, operateStore, fileStoreToken))
}

func loadConfig(filename string, path string) {
	viper.SetConfigFile(filename)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("could not read config")
	}
}

func failOnEmpty(value ...string) {
	for _, v := range value {
		if strings.TrimSpace(v) == "" {
			log.Fatal("some value is missing")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
