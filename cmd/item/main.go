package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/esd-gpos/store-inventory-stream/item_stream"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strings"
	"time"
)

const (
	itemFilename = "item-token"
	configFile   = "main.env"
	configPath   = "."
)

func main() {
	// Load config
	loadConfig(configFile, configPath)

	// Get env variables
	mongoStoreItemUri := viper.GetString("MONGO_STORE_ITEM_URI")
	mongoStoreItemDb := viper.GetString("MONGO_STORE_ITEM_DB")
	mongoItemCol := viper.GetString("MONGO_ITEM_COLLECTION")

	mongoTranUri := viper.GetString("MONGO_TRAN_URI")
	mongoTranDb := viper.GetString("MONGO_TRAN_DB")
	mongoTranItemCol := viper.GetString("MONGO_TRAN_ITEM_COLLECTION")

	failOnEmpty(mongoStoreItemUri, mongoStoreItemDb, mongoItemCol, mongoTranUri, mongoTranDb, mongoTranItemCol)

	// Connect to Mongo Store and Item collection
	mongoStoreItemClient, err := mongo.NewClient(options.Client().ApplyURI(mongoStoreItemUri))
	failOnError("could not create mongo client", err)

	timeout, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoStoreItemClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoStoreCollection := mongoStoreItemClient.Database(mongoStoreItemDb).Collection(mongoItemCol)

	// Connect to Mongo Transaction Store and Item collection
	mongoTranClient, err := mongo.NewClient(options.Client().ApplyURI(mongoTranUri))
	failOnError("could not create mongo client", err)

	timeout, cancel = context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoTranClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoTranItemCollection := mongoTranClient.Database(mongoTranDb).Collection(mongoTranItemCol)

	// Create Repo
	operateItem := &item_stream.MongoOperateRepo{Col: mongoTranItemCollection}
	fileItemToken := &item_stream.FileTokenRepo{Filename: itemFilename}

	log.Println("Begin")
	log.Fatal(item_stream.WatchMongoStream(context.Background(), mongoStoreCollection, operateItem, fileItemToken))
}

func loadConfig(filename string, path string) {
	viper.SetConfigFile(filename)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("could not read config")
	}
}

func failOnEmpty(value ...string) {
	for _, v := range value {
		if strings.TrimSpace(v) == "" {
			log.Fatal("some value is missing")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
